var gulp         = require('gulp');
// var browserSync  = require('browser-sync');
var cleanCSS = require('gulp-clean-css');
var rename   = require("gulp-rename");
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var mozjpeg = require('imagemin-mozjpeg');
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulp         = require('gulp');
var sass         = require('gulp-sass');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var compass = require('gulp-compass');

// gulp.task('browser-sync', function() {
//   browserSync.init({
//       server: {
//           baseDir: "./",
//           index: "index.html"
//       }
//   });
// });

// gulp.task('bs-reload', function () {
//   browserSync.reload();
// });

// gulp.task( 'default', gulp.series( gulp.parallel( 'browser-sync' ) ), function() {
//   gulp.watch( './*.html', gulp.task( 'bs-reload' ) );
//   gulp.watch( './css/*.css', gulp.task( 'bs-reload' ) );
//   gulp.watch( './js/*.js', gulp.task( 'bs-reload' ) );
// });

gulp.task('default', function() {
  return gulp.src("css/*.css")
    .pipe(cleanCSS())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('css/'));
});

var uglify = require('gulp-uglify');
var rename = require("gulp-rename");

gulp.task('default', function() {
  return gulp.src("js/*.js")
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('js/'));
});

var imageminOption = [
  pngquant({ quality: [0.65,0.8], }),
  mozjpeg({ quality: 80 }),
  imagemin.gifsicle({
  interlaced: false,
  optimizationLevel: 1,
  colors: 256
  }),
  imagemin.jpegtran(),
  imagemin.optipng(),
  imagemin.svgo()
  ];

  gulp.task('default', function () {
    return gulp
      .src('./img/base/*.{png,jpg,gif,svg}')
      .pipe(imagemin(imageminOption))
      .pipe(gulp.dest('./img'));
    });

    gulp.task('sass', function() {
    return gulp.src('./sass/**/*.scss')
      .pipe(sass({outputStyle: 'expanded'}))
      .pipe(gulp.dest('./css'));
    });

    gulp.task('watch', function() {
      gulp.watch('./sass/**/*.scss', gulp.task( 'sass' ) );
    });

    gulp.task('sass', function() {
    return gulp.src('./sass/**/*.scss')
      .pipe(sass({outputStyle: 'expanded'}))
      .pipe(postcss([autoprefixer()]))
      .pipe(gulp.dest('./css'));
    });
    
    
      gulp.task('default', function(){
      gulp.src('sass/**/*.scss').pipe(compass({
      config_file: 'config.rb',
      comments: false,
      css: 'stylesheets/',
      sass: 'sass/'
      }));
      });


    var plumber = require('gulp-plumber'); //エラー時の強制終了を防止
    var notify = require('gulp-notify'); //エラー発生時にデスクトップ通知する
    
    gulp.task('sass', function() {
    return gulp.src('./sass/**/*.scss')
    .pipe(plumber({errorHandler: notify.onError('<%= error.message %>')}))
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(gulp.dest('./css'));
    });

    gulp.task('default', function () {
    gulp.watch("sass/**/*.scss",["sass"]);
    });

    gulp.task( 'watch', function(done) {
      gulp.watch( './src/scss/**/*.scss', gulp.task('sass') ); //sassが更新されたらgulp sassを実行
      // gulp.watch('./src/scss/**/*.scss', gulp.task('bs-reload')); //sassが更新されたらbs-reloadを実行
      // gulp.watch( './src/js/*.js', gulp.task('bs-reload') ); //jsが更新されたらbs-relaodを実行
      });
      
      // default
      gulp.task('default', gulp.series(gulp.parallel('watch')));
      // gulp.task('default', gulp.series(gulp.parallel('browser-sync', 'watch')));

